import React, {Component} from 'react';
import MakeAppointmentDetails from './MakeAppointmentDetails';

class MakeAppointmentPage extends Component {
    state = {};
    render() {
      return (
        <div className='container'>
            <div className='row'>
                <div className='section center blue-text'>
                    <h4 className='left'>Make an Appointment</h4>
                </div>
                <br/><br/><br/><br/>
                <div className='section col s12'>
                  <MakeAppointmentDetails/>
                </div>
            </div>
        </div>
      );
    };
}

export default MakeAppointmentPage;