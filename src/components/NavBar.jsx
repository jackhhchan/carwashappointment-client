import React, {Component} from 'react';
import {Link} from 'react-router-dom'

class NavBar extends Component {
    state = {};
    render(){
        return (
            <nav>
            <div className="nav-wrapper blue">
                {/* <span className="blue-text">fo</span> */}
                <div className='container'>
                    <Link to="/" className="brand-logo left"><i className='material-icons'>local_car_wash</i>Gabriel &amp; Davids' Car Wash</Link>
                    <ul id="nav-mobile" className="right hide-on-med-and-down">
                        <li key="makeappointment"><Link to="/makeappointment">Make Appointment</Link></li>
                        <li key="reschedule"><Link to="/reschedule">Reschedule</Link></li>
                        <li key="info"><Link to="/info">Info</Link></li>
                    </ul>
                </div>
            </div>
            </nav>
        );
    }
}


export default NavBar;