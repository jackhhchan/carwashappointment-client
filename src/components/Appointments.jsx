import React, { Component } from "react";
import Login from "./Login";
import Register from "./Register";
import PersonalInfo from "./PersonalInfo";
import CarInfo from "./CarInfo";
import { Button } from "react-materialize";
import { Icon } from "react-materialize";

class Appointments extends Component {
  state = {};
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="section left blue-text">
            <h4>My Information</h4>
          </div>
          <br />
          <br />
          <br />
        </div>
        <table>
          <thead>
            <tr>
              <th>Date and Time</th>
              <th>Car</th>
              <th>Washing Option</th>
              <th>Your Comment</th>
              <th>Actions</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td>*PlaceHolder*</td>
              <td>*PlaceHolder*</td>
              <td>*PlaceHolder*</td>
              <td>
                <button
                  class="btn waves-effect waves-light"
                  type="submit"
                  name="action"
                >
                  re-schedule
                  <i class="material-icons right"></i>
                </button>
                <br />
                <br />
                <button
                  class="btn waves-effect waves-light"
                  type="submit"
                  name="action"
                >
                  cancel
                  <i class="material-icons right"></i>
                </button>
              </td>
            </tr>
            <tr>
              <td>*PlaceHolder*</td>
              <td>*PlaceHolder*</td>
              <td>*PlaceHolder*</td>
              <td>*PlaceHolder*</td>
            </tr>
            <tr>
              <td>*PlaceHolder*</td>
              <td>*PlaceHolder*</td>
              <td>*PlaceHolder*</td>
              <td>*PlaceHolder*</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default Appointments;
