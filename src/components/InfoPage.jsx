import React, { Component } from "react";
import Login from "./Login";
import Register from "./Register";
import PersonalInfo from "./PersonalInfo";
import CarInfo from "./CarInfo";
import { Button } from "react-materialize";
import { Icon } from "react-materialize";

class InfoPage extends Component {
  state = {};
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="section left blue-text">
            <h4>My Information</h4>
            <h6>
              Add information about you and your cars in order to create a car
              wash appointment
            </h6>
          </div>
          <br />
          <br />
          <br />
          <div className="center">
            <PersonalInfo />
          </div>
          <div className="center">
            <CarInfo />
          </div>
          <div className="center">
            <Button className='blue' node="a" waves="light" large style={{ marginTop: "30px" }}>
              Save
            </Button>
          </div>
        </div>
      </div>
    );
  }


}

export default InfoPage;
