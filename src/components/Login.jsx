import React, {Component} from 'react';

class Login extends Component {
    state = {
        email: '',
        password: ''
    };
    
    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <h5 className='blue-text col s12'>Log In</h5>
                    <br/><br/>
                </div>
                <div className='row'>                
                    <form className='col s12' onSubmit={this.handleSubmit}>
                        <div className='row'>
                            <div className='input-field col s12'>
                                <input placeholder="user@email.com"type='email' id='email' className='validate' onChange={this.handleInput}/> {/*'validate' for highlights, check materialize*/}
                                <label id='email' className='active'>Email:</label>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='input-field col s12'>
                                <input placeholder="" type='password' id='password' className='validate' onChange={this.handleInput}/> {/*'validate' for highlights, check materialize*/}
                                <label id='password' className='active'>Password:</label>
                                <span className="helper-text" data-error="wrong" data-success={this.state.password + " =)"}></span>
                            </div>
                        </div>
                        <button className="btn blue waves-effect waves-light" type="submit">Login</button>
                    </form>
                </div>
            </div>
        );
    };

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state);
    }

    handleInput = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }
}

export default Login;