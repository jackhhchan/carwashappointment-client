import React, {Component} from 'react';
import Login from './Login';
import Register from './Register';

class InitPage extends Component {
    state = {};
    render() {
      return (
        <div className='container'>
            <div className='row'>
                <div className='section center blue-text'>
                    <h4>Welcome to Gabriel and David's Car Wash</h4>
                    <h6>Prior to using our service you must register and be logged-in</h6>
                </div>
                <div className='section'>
                    <div className='col s6'>
                        <Login/>
                    </div>
                    <div className='col s6'>
                        <Register/>
                    </div>
                </div>
            </div>
        </div>
      );
    };
}

export default InitPage;