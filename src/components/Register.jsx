import React, {Component} from 'react';

class Register extends Component {
    state = {
        email: '',
        password: '',
        confirmPassword: ''
    };
    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <h5 className='blue-text col s12'>Register</h5>
                    <p className='blue-text col s12'>Only for Melbourne customers</p>
                    <br/><br/>
                </div>
                <div className='row'>                
                    <form className='col s12' onSubmit={this.handleSubmit}>
                        <div className='row'>
                            <div className='input-field col s12'>
                                <input placeholder="user@email.com"type='email' id='remail' className='validate' onChange={this.handleInput}/> {/*'validate' for highlights, check materialize*/}
                                <label id='remail' className='active'>Email:</label>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='input-field col s12'>
                                <input placeholder="" type='password' id='rpassword' className='validate' onChange={this.handleInput}/> {/*'validate' for highlights, check materialize*/}
                                <label id='rpassword' className='active'>Password:</label>
                                <span className="helper-text" data-error="wrong" data-success={this.state.password + " =)"}></span>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='input-field col s12'>
                                <input placeholder="" type='password' id='rcPassword' className='validate' onChange={this.handleInput}/> {/*'validate' for highlights, check materialize*/}
                                <label id='rcPassword' className='active'>Confirm password:</label>
                                <span className="helper-text" data-error="wrong" data-success={this.state.password + " =)"}></span>
                            </div>
                        </div>
                        <button className="btn blue waves-effect waves-light" type="submit">Register</button>
                    </form>
                </div>
            </div>
        );
    }

    row(type, id, placeholder, label) {
        return (
            <div className='row'>
                <div className='input-field col s12'>
                    <input placeholder={placeholder} type={type} id={id} className='validate' onChange={this.handleInput}/> {/*'validate' for highlights, check materialize*/}
                    <label id={id} className='active'>{label}:</label>
                </div>
            </div>
        );
    };
}


export default Register;