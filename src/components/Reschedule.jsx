import React, {Component} from 'react';
// import {Select} from 'react-materialize';
import {Select} from 'react-materialize';

class Reschedule extends Component {
    state = {
        times: this.availableTimes(10),      //TODO: format time range using filter()
        selectedDate: "",
        selectedTime: "",
    };

    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <h4 className='blue-text'>Reschedule</h4>
                </div>
                <div className='row'>                
                    <form className='col s12' onSubmit={this.handleSubmit}>
                        <div className='row'>{this.row('date', 'selectedDate', " ", "Date")}</div>
                        <div className='row'>{this.select("Time", this.state.times, 'selectedTime')}</div>
                        <div className='row center col s12'>
                            <button className="btn-large blue waves-effect waves-light" type="submit">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    };

    
    // HELPER FUNCTIONS -- 
    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state);
        // console.log(e);
    };

    handleInput = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    };

    row(type, id, placeholder, label) {
        return (
            <div className='input-field col s12'>
                <input placeholder={placeholder} type={type} id={id} className='validate' onChange={this.handleInput}/> {/*'validate' for highlights, check materialize*/}
                <label id={id} className='active'>{label}:</label>
            </div>
        );
    };

    select(label, list, id){
        return (
            <Select label={label} l={12} id={id} className='blue' validate={true} onChange={this.handleInput}>
                {list.map(t => <option value={t}>{t}</option>)}
            </Select>
        )
    }

    /// returns the available times in 10min increments
    availableTimes(increment) {
        var times = [];
        var hours = [9, 10, 11, 12, 1, 2, 3, 4, 5, 6];
        for (var i=0; i<hours.length; i++){
            if (i===hours.length-1) {
                times.push(`${hours[i]}:00`)
                break
            }
            var min = 0;
            while (min < 60){
                if (min === 0){times.push(`${hours[i]}:${min}0`)}
                else {times.push(`${hours[i]}:${min}`)}
                min += increment;

            }
        }
        return times;
    }
}

export default Reschedule;