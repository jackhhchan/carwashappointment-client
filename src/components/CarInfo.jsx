import React from "react";
import { Select } from "react-materialize";
class CarInfo extends React.Component {
  state = {
    cars: [{ carPlate: "", carType: "" }]
  };
  handleChange = e => {
    if (["carPlate", "carType"].includes(e.target.className)) {
      let cars = [...this.state.cars];
      cars[e.target.dataset.id][
        e.target.className
      ] = e.target.value.toUpperCase();
      this.setState({ cars }, () => console.log(this.state.cars));
    } else {
      this.setState({ [e.target.carPlate]: e.target.value.toUpperCase() });
    }
  };
  addCar = e => {
    this.setState(prevState => ({
      cars: [...prevState.cars, { carPlate: "", carType: "" }]
    }));
  };
  handleSubmit = e => {
    e.preventDefault();
  };
  render() {
    let { cars } = this.state;
    return (
      <div className="container">
        <form onSubmit={this.handleSubmit} onChange={this.handleChange}>
          <div className="row">
            <h5 className="blue-text col s12">Car Details</h5>
            <br />
            <br />
          </div>
          {cars.map((val, idx) => {
            let carId = `car-${idx}`,
              typeId = `type-${idx}`;
            return (
              <div className="row">
                <form className="col s12">
                  <div className="row">
                    <div className="input-field col s12">
                      <input
                        className=""
                        type="text"
                        carPlate={carId}
                        data-id={idx}
                        id={carId}
                        value={cars[idx].carPlate}
                        className="carPlate"
                      />
                      <label htmlFor={carId}>{`#${idx +
                        1} Car's Plate Number`}</label>
                    </div>
                  </div>

                  <div className="row">
                    <div className="">
                      <Select label="Car's Type" value="2" l="12">
                        <option value="" disabled selected>
                          Choose your car type
                        </option>
                        <option value="1">Hatchback</option>
                        <option value="2">Sedan</option>
                        <option value="3">SUV</option>
                      </Select>
                    </div>
                  </div>
                </form>
              </div>
            );
          })}
          <button
            className="btn-small blue waves-effect waves-light"
            onClick={this.addCar}
          >
            ADD ADDITIONAL CAR
          </button>
        </form>
      </div>
    );
  }
}
export default CarInfo;

// import React, { Component } from "react";
// import { Select } from "react-materialize";
// import { Button } from "react-materialize";
// import { Icon } from "react-materialize";

// class CarInfo extends Component {
//   state = {
//     carPlate: "",
//     cartype: ""
//   };

//   render() {
//     return (
//       <div className="container">
//         <div className="row">
//           <h5 className="blue-text col s12">Your Car's Details</h5>
//           <br />
//           <br />
//         </div>

//         <div className="row">
//           <form className="col s12" onSubmit={this.handleSubmit}>
//             <div className="row">
//               <div className="input-field col s12">
//                 <input
//                   placeholder=""
//                   type="email"
//                   id="email"
//                   className="validate"
//                   onChange={this.handleInput}
//                 />{" "}
//                 {/*'validate' for highlights, check materialize*/}
//                 <label id="email">Plate Number:</label>
//               </div>
//             </div>
//           </form>
//         </div>

//         <div className="row">
//           <div className="col s12">
//             <div className="row">
//               <div className="input-field col s12">
// <Select label="Choose your option" value="2" l="12">
//   <option value="" disabled selected>
//     Choose your car type
//   </option>
//   <option value="1">Hatchback</option>
//   <option value="2">Sedan</option>
//   <option value="3">SUV</option>
// </Select>
//               </div>
//             </div>
//           </div>
//         </div>
//         <div>
//           <Button className='blue' waves="light" style={{ marginRight: "5px" }}>
//             Add additional car
//             <Icon left>add</Icon>
//           </Button>
//         </div>
//       </div>
//     );
//   }

//   handleSubmit = e => {
//     e.preventDefault();
//     console.log(this.state);
//   };

//   handleInput = e => {
//     this.setState({
//       [e.target.id]: e.target.value
//     });
//   };
// }

// export default CarInfo;
