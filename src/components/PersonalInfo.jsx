import React, { Component } from "react";

class PersonalInfo extends Component {
  state = {
    fullname: "",
    address: "",
    postCode: "",
    phoneMobile: "",
    phoneHome: "",
    phoneWork: ""
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <h5 className="blue-text col s12">Personal Details</h5>
          <br />
          <br />
        </div>
        <div className="row">
          <div className="input-field col s12">
            <label>{`Full Name`}</label>
            <input type="text" />
          </div>
        </div>
        <div className="row">
          <div className="input-field col s12">
            <label>{`Address`}</label>
            <input type="text" />
          </div>
        </div>
        <div className="row">
          <div className="input-field col s12">
            <label>{"Post Code"}</label>
            <input type="text" />
          </div>
        </div>
        <div className="row">
          <div className="input-field col s12">
            <label>{"Phone Number (mobile)"}</label>
            <input type="text" />
          </div>
        </div>
        <div className="row">
          <div className="input-field col s12">
            <label>{"Phone Number (home)"}</label>
            <input type="text" />
          </div>
        </div>
        <div className="row">
          <div className="input-field col s12">
            <label>{"Phone Number (work)"}</label>
            <input type="text" />
          </div>
        </div>
      </div>
    );
  }

  handleSubmit = e => {
    e.preventDefault();
    console.log(this.state);
  };

  handleInput = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };
}

const form = (
  <form className="blue-text">
    <label>Testing</label>
  </form>
);

export default PersonalInfo;
