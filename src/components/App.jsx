import React from "react";
import { BrowserRouter, Route } from "react-router-dom";

import NavBar from "./NavBar";
import InitPage from "./InitPage";
import MakeAppointmentPage from "./MakeAppointmentPage";
import Reschedule from "./Reschedule";
import InfoPage from "./InfoPage";

function App() {
  return (
    <React.Fragment>
      <BrowserRouter>
        <NavBar />
        <Route exact path="/" component={InitPage} />
        <Route path="/makeappointment" component={MakeAppointmentPage} />
        <Route path="/reschedule" component={Reschedule} />
        <Route path="/info" component={InfoPage} />
      </BrowserRouter>
      {/* <InitPage/> */}
    </React.Fragment>
  );
}

export default App;
